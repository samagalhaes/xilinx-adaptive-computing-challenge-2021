from retinanet import *
import numpy as np

from tensorflow_model_optimization.quantization.keras import vitis_quantize

import os
import logging


checkpoints_path = 'checkpoints_vitis/Aveleda_SGD_quantised/retinanet_resnest50_coco_03'

float_dir = "Aveleda_SGD/quantized"
model_filename = "quantized_qat"

if not os.path.isdir(float_dir):
   os.makedirs(float_dir)


logging.info("Loading model")
num_classes = 80

# model = RetinaNet(num_classes, training=False)
# quantizer = vitis_quantize.VitisQuantizer(model, quantize_strategy='8bit_tqt')
# model = quantizer.get_qat_model()

# latest_checkpoint = checkpoints_path # tf.train.latest_checkpoint(checkpoints_path)
# assert latest_checkpoint, "[FATAL] Any checkpoint weights available in ".format(checkpoints_path)
# print('[INFO] Loading weights from {}'.format(latest_checkpoint))
# model.load_weights(latest_checkpoint).expect_partial()

# quantized_model = vitis_quantize.VitisQuantizer.get_deploy_model(model)

model = tf.keras.models.load_model(checkpoints_path, custom_objects={"RetinaNetLoss": RetinaNetLoss}, compile=False)
outputs = [model.layers[-3].output, model.layers[-2].output]
print(outputs)
model = tf.keras.Model(inputs = model.input, outputs = outputs)

quantized_model = vitis_quantize.VitisQuantizer.get_deploy_model(model)

print("[INFO] Printing model summary and ploting graph to file")
quantized_model.summary()
tf.keras.utils.plot_model(quantized_model, to_file=os.path.join(float_dir, 'retinanet_resnet50.png'), show_shapes=True, dpi=96)

model_path = os.path.join(float_dir, model_filename+".h5")
print("[INFO] Saving HDF5 model to {}".format(model_path))
quantized_model.save(model_path, save_format='h5')

