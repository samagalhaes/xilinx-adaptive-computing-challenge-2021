from retinanet import *
import numpy as np

import os


checkpoints_path = 'checkpoints/coco'

float_dir = "float"
model_filename = "float"

if not os.path.isdir(float_dir):
   os.makedirs(float_dir)

num_classes = 80

model = RetinaNet(num_classes, training=False)
latest_checkpoint = tf.train.latest_checkpoint(checkpoints_path)
assert latest_checkpoint, "[FATAL] Any checkpoint weights available in ".format(checkpoints_path)
print('[INFO] Loading weights from {}'.format(latest_checkpoint))
model.load_weights(latest_checkpoint).expect_partial()

print("[INFO] Printing model summary and ploting graph to file")
model.summary()
tf.keras.utils.plot_model(model, to_file='retinanet_resnet50.png', show_shapes=True, dpi=300)

model_path = os.path.join(float_dir, model_filename+".h5")
print("[INFO] Saving HDF5 model to {}".format(model_path))
model.save(model_path, save_format='h5')

