import numpy as np
from nms.nms import nms
import logging
logging.basicConfig(format='[POSTPROCESS %(levelname)s] %(message)s', level=logging.DEBUG)

from vaitrace_py import vai_tracepoint

def read_classes (file):
    with open(file, "r") as f:
        classes = f.read().splitlines()
    return classes

categories_names = read_classes("aveleda_labels.txt")

"""
## Implementing Anchor generator

Anchor boxes are fixed sized boxes that the model uses to predict the bounding
box for an object. It does this by regressing the offset between the location
of the object's center and the center of an anchor box, and then uses the width
and height of the anchor box to predict a relative scale of the object. In the
case of RetinaNet, each location on a given feature map has nine anchor boxes
(at three scales and three ratios).
"""

class AnchorBox:
    """Generates anchor boxes.

    This class has operations to generate anchor boxes for feature maps at
    strides `[8, 16, 32, 64, 128]`. Where each anchor each box is of the
    format `[x, y, width, height]`.

    Attributes:
      aspect_ratios: A list of float values representing the aspect ratios of
        the anchor boxes at each location on the feature map
      scales: A list of float values representing the scale of the anchor boxes
        at each location on the feature map.
      num_anchors: The number of anchor boxes at each location on feature map
      areas: A list of float values representing the areas of the anchor
        boxes for each feature map in the feature pyramid.
      strides: A list of float value representing the strides for each feature
        map in the feature pyramid.
    """
    def __init__(self):
        self.aspect_ratios = [0.5, 1.0, 2.0]
        self.scales = [2 ** x for x in [0, 1 / 3, 2 / 3]]

        self._num_anchors = len(self.aspect_ratios) * len(self.scales)
        self._strides = [2 ** i for i in range(3, 8)]
        self._areas = [x ** 2 for x in [32.0, 64.0, 128.0, 256.0, 512.0]]
        self._anchor_dims = self._compute_dims()

    def _compute_dims(self):
        """Computes anchor box dimensions for all ratios and scales at all levels
        of the feature pyramid.
        """
        anchor_dims_all = []
        for area in self._areas:
            anchor_dims = []
            for ratio in self.aspect_ratios:
                anchor_height = np.sqrt(area / ratio)
                anchor_width = area / anchor_height
                dims = np.reshape(
                    np.stack([anchor_width, anchor_height], axis=-1), [1, 1, 2]
                )
                for scale in self.scales:
                    anchor_dims.append(scale * dims)
            anchor_dims_all.append(np.stack(anchor_dims, axis=-2))
        return anchor_dims_all

    def _get_anchors(self, feature_height, feature_width, level):
        """Generates anchor boxes for a given feature map size and level

        Arguments:
          feature_height: An integer representing the height of the feature map.
          feature_width: An integer representing the width of the feature map.
          level: An integer representing the level of the feature map in the
            feature pyramid.

        Returns:
          anchor boxes with the shape
          `(feature_height * feature_width * num_anchors, 4)`
        """
        #rx = tf.range(feature_width, dtype=tf.float32) + 0.5
        #ry = tf.range(feature_height, dtype=tf.float32) + 0.5
        
        feature_width = int(feature_width)
        feature_height = int(feature_height)

        rx = np.array(range(0, feature_width) , dtype=np.float32) + 0.5
        ry = np.array(range(0, feature_height), dtype=np.float32) + 0.5
        
        centers = np.stack(np.meshgrid(rx, ry), axis=-1) * self._strides[level - 3]
        centers = np.expand_dims(centers, axis=-2)
        centers = np.tile(centers, [1, 1, self._num_anchors, 1])
        dims = np.tile(
            self._anchor_dims[level - 3], [feature_height, feature_width, 1, 1]
        )
        anchors = np.concatenate((centers, dims), axis=-1)
        return np.reshape(
            anchors, [feature_height * feature_width * self._num_anchors, 4]
        )

    def get_anchors(self, image_height, image_width):
        """Generates anchor boxes for all the feature maps of the feature pyramid.

        Arguments:
          image_height: Height of the input image.
          image_width: Width of the input image.

        Returns:
          anchor boxes for all the feature maps, stacked as a single tensor
            with shape `(total_anchors, 4)`
        """
        anchors = [
            self._get_anchors(
                np.ceil(image_height / 2 ** i),
                np.ceil(image_width / 2 ** i),
                i,
            )
            for i in range(3, 8)
        ]
        return np.concatenate(anchors, axis=0)


class DecodePredictions():
    """A Keras layer that decodes predictions of the RetinaNet model.

    Attributes:
      num_classes: Number of classes in the dataset
      confidence_threshold: Minimum class probability, below which detections
        are pruned.
      nms_iou_threshold: IOU threshold for the NMS operation
      max_detections_per_class: Maximum number of detections to retain per
       class.
      max_detections: Maximum number of detections to retain across all
        classes.
      box_variance: The scaling factors used to scale the bounding box
        predictions.
    """

    def __init__(
        self,
        num_classes=80,
        confidence_threshold=0.05,
        nms_iou_threshold=0.5,
        max_detections_per_class=100,
        max_detections=100,
        box_variance=[0.1, 0.1, 0.2, 0.2],
        **kwargs
    ):
        logging.info("Creating predictions decoder")
        #super(DecodePredictions, self).__init__(**kwargs)
        self.num_classes = num_classes
        self.confidence_threshold = confidence_threshold
        self.nms_iou_threshold = nms_iou_threshold
        self.max_detections_per_class = max_detections_per_class
        self.max_detections = max_detections

        self.anchor_box = AnchorBox().get_anchors(384, 384)
        self._box_variance = np.array(
            [0.1, 0.1, 0.2, 0.2], dtype=np.float32
        )

    def nms (self, boxes, classes, scores, thd_iou = 0.3):
        if len(boxes) <= 0:
            return [], [], []
        
        areas = (boxes[:,2] - boxes[:,0]) * (boxes[:,3] - boxes[:,1])
        order = np.array(scores).argsort()
        
        fboxes = []
        fclasses = []
        fscores = []
        while len(order > 0):
            idx = order[-1]
            bb = boxes[idx]
            
            fboxes.append(boxes[idx])
            fclasses.append(classes[idx])
            fscores.append(scores[idx])
            
            order = order[:-1]
            
            if len(order) == 0:
                break
            
            ordered_bb = np.array([boxes[i] for i in order])
            
            intersection = np.concatenate([np.maximum(ordered_bb[:,:2], bb[:2]), 
                                        np.minimum(ordered_bb[:,2:], bb[2:])], 1)
            w,h = [intersection[:,2]-intersection[:,0],
                intersection[:,3]-intersection[:,1]]
            w = np.clip(w, 0.0, None)
            h = np.clip(h, 0.0, None)
            
            intersection_area = w*h
            ordered_areas = np.array([areas[i] for i in order])
            union = (ordered_areas - intersection_area) + areas[idx]
            iou = intersection_area / union
            
            mask = iou < thd_iou
            order = order[mask]
            
        return fboxes, fclasses, fscores

    def convert_to_corners(self, boxes):
        """Changes the box format to corner coordinates

        Arguments:
        boxes: A tensor of rank 2 or higher with a shape of `(..., num_boxes, 4)`
            representing bounding boxes where each box is of the format
            `[x, y, width, height]`.

        Returns:
        converted boxes with shape same as that of boxes.
        """
        return np.concatenate(
            [boxes[..., :2] - boxes[..., 2:] / 2.0, boxes[..., :2] + boxes[..., 2:] / 2.0],
            axis=-1,
    )

    def convert_to_xywh(self, boxes):
        """Changes the box format to left, top, width and height coordinates

        Arguments:
        boxes: A tensor of rank 2 or higher with a shape of `(..., num_boxes, 4)`
            representing bounding boxes where each box is of the format
            `[xmin, ymin, xmax, ymax]`.

        Returns:
        converted boxes with shape same as that of boxes.        
        """

        boxes[..., :2] = boxes[..., :2] - boxes[..., 2:] * 0.5
        return boxes


    @vai_tracepoint
    def _decode_box_predictions(self, anchor_boxes, box_predictions, scale_loc):
        if not(box_predictions.shape[0]):
            return np.array([])
        
        boxes = box_predictions * self._box_variance
        boxes = np.concatenate(
            [
                boxes[:, :2] * scale_loc * anchor_boxes[:, 2:] + anchor_boxes[:, :2], # cx, cy
                np.exp(boxes[:, 2:] * scale_loc) * anchor_boxes[:, 2:], # w, h
            ],
            axis=-1,
        )
        boxes_transformed = self.convert_to_xywh(boxes)
        return boxes_transformed

    @vai_tracepoint
    def sigmoid(self, x):
        """Computes the sigmoid function of a number

        Arguments:
        x: Number to compute the sigmoid

        Returns:
        Sigmoid result        
        """
        return 1 / (1 + np.exp(-x))

    @vai_tracepoint
    def call(self, predictions, output_scale, image_id):
        scale_conf = output_scale[1]
        cls_predictions = predictions[1][0] #self.sigmoid(predictions[1] * scale_conf)[0]

        classes = np.argmax(cls_predictions, axis=1)
        scores = np.take_along_axis(cls_predictions, np.array(classes)[np.newaxis].T, axis=1).flatten()

        threshold = - np.log(1/(0.3) -1) / scale_conf
        thresholded_index = np.where(scores > threshold)

        scores = self.sigmoid(scores[thresholded_index] * scale_conf)
        classes = classes[thresholded_index]
        
        box_predictions = predictions[0][0][thresholded_index]

        
        boxes = self._decode_box_predictions(self.anchor_box[thresholded_index], box_predictions, output_scale[0]) / 384.0

        best_index = nms.boxes(boxes, scores,
                               nms_algorithm=nms.fast.nms,
                               score_threshold=0.05,
                               top_k = 0)

        results = []
        if image_id != None:
            for bbox, score, classe in zip (boxes[best_index], scores[best_index], classes[best_index]):
                result = {
                    "image_id"     : str(image_id),
                    "category_id"  : int(classe),
                    "category_name": categories_names[classe], 
                    "bbox"       : bbox.tolist(),
                    "score"       : float(score),
                }
                results.append(result)

        return results
