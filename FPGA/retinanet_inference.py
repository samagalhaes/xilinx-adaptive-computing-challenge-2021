from ctypes import *
from typing import List
import cv2
import numpy as np
import xir
import vart
import os
import math
import threading
import time
import sys
import logging
import pathlib
logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

import postprocessing

"""
## Preprocessing Data
"""
_B_MEAN = 103.939
_G_MEAN = 116.779
_R_MEAN = 123.68
MEANS = [_B_MEAN, _G_MEAN, _R_MEAN]
std = None
SCALES = [1.0, 1.0, 1.0]    

def preprocessing(img_path, fix_scale, width=384, height=384):
    means = MEANS
    scales = SCALES

    img = cv2.imread(img_path)
    img = cv2.resize(img, (width, height))
    
    B, G, R = cv2.split(img)
    B = (B - means[0]) * scales[0] * fix_scale
    G = (G - means[1]) * scales[1] * fix_scale
    R = (R - means[2]) * scales[2] * fix_scale
    img = cv2.merge([B, G, R])
    img = img.astype(np.int8)

    return img


"""
## DPU Runner
"""

def runRetinaNet(runner: "Runner", img_path, input_scale, decoder = None):
    # Get tensor
    input_tensors  = runner.get_input_tensors()
    output_tensors = runner.get_output_tensors()
    
    input_ndim = tuple(input_tensors[0].dims)
    # pre_output_size = int(output_tensors[0].get_data_size() / input_ndim[0])

    bboxes_output_ndim = tuple(output_tensors[0].dims)
    bboxes_output_fixpos = output_tensors[0].get_attr("fix_point")
    bboxes_output_scale = 2**(-1.0 * bboxes_output_fixpos)

    scores_output_ndim = tuple(output_tensors[1].dims)
    scores_output_fixpos = output_tensors[1].get_attr("fix_point")
    scores_output_scale = 2**(-1.0 * scores_output_fixpos)

    """prepare batch input/output """
    input_data = [np.empty(input_ndim, dtype=np.int8, order="C")]
    bboxes_output_data = np.empty(bboxes_output_ndim, dtype=np.int8, order="C")
    scores_output_data = np.empty(scores_output_ndim, dtype=np.int8, order="C")
    
    """init input image to input buffer """
    image_run = input_data[0]
    image_run[0, ...] = preprocessing(img_path, input_scale).reshape(input_ndim[1:])
    """run with batch """
    job_id = runner.execute_async(input_data, [bboxes_output_data, scores_output_data])
    runner.wait(job_id)
    
    if not decoder == None:
        result = decoder.call([bboxes_output_data, scores_output_data], 
                              [bboxes_output_scale, scores_output_scale], 
                              str(pathlib.Path(img_path).stem))
    else:
        result = None

    return result


"""
## Obtain dpu subgrah
"""
def get_child_subgraph_dpu(graph: "Graph") -> List["Subgraph"]:
    """
    obtain the DPU subgraph
    """
    assert graph is not None, "'graph' should not be None."
    root_subgraph = graph.get_root_subgraph()
    assert (root_subgraph
            is not None), "Failed to get root subgraph of input Graph object."
    if root_subgraph.is_leaf:
        return []
    child_subgraphs = root_subgraph.toposort_child_subgraph()
    assert child_subgraphs is not None and len(child_subgraphs) > 0
    return [
        cs for cs in child_subgraphs
        if cs.has_attr("device") and cs.get_attr("device").upper() == "DPU"
    ]

def main(argv):
    test_images_path = "aveleda_test"
    thread_num = 1
    threadAll = []

    # reading the graph
    logging.info("Loading the model")
    graph = xir.Graph.deserialize("retinanet_quantized_ptq_aveleda/retinanet_resnet50_aveleda_384_384_3.xmodel")
    subgraphs = get_child_subgraph_dpu(graph)
    assert len(subgraphs) == 1, "RetinaNet model can only have a single DPU graph. It has {}".format(len(subgraphs))
    all_dpu_runners = []
    for i in range(int(thread_num)):
        all_dpu_runners.append(vart.Runner.create_runner(subgraphs[0], "run"))

    input_fixpos = all_dpu_runners[0].get_input_tensors()[0].get_attr("fix_point")
    input_scale = 2**(1.0 * input_fixpos)

    logging.info("Loading images")
    list_images = os.listdir(test_images_path)
    img = [os.path.join(test_images_path, img_path) for img_path in list_images] 

    decoder = postprocessing.DecodePredictions()

    results = []
    for img_path in img:
        print(img_path)
        result = runRetinaNet(all_dpu_runners[0], img_path, input_scale, decoder)
        results.extend(result)
    # print(len(results))

    import json
    # print(results)
    with open('retinanet_quantized_ptq_aveleda/results.json', 'w') as f:
        json.dump(results, f)

    del all_dpu_runners


if __name__ == '__main__':
    main(sys.argv)
