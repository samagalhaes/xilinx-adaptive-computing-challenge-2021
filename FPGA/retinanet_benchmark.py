#!/usr/bin/python3

from ctypes import *
from typing import List
import cv2
import numpy as np
import xir
import vart
import os
import math
import threading
import time
import sys
import logging
logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

import postprocessing

from vaitrace_py import vai_tracepoint

"""
## Preprocessing Data
"""
_B_MEAN = 103.939
_G_MEAN = 116.779
_R_MEAN = 123.68
MEANS = [_B_MEAN, _G_MEAN, _R_MEAN]
std = None
SCALES = [1.0, 1.0, 1.0]    

@vai_tracepoint
def preprocessing(img_path, fix_scale, width=384, height=384):
    means = MEANS
    scales = SCALES

    img = cv2.imread(img_path)
    img = cv2.resize(img, (width, height))

    B, G, R = cv2.split(img)
    B = (B - means[0]) * scales[0] * fix_scale
    G = (G - means[1]) * scales[1] * fix_scale
    R = (R - means[2]) * scales[2] * fix_scale
    img = cv2.merge([B, G, R])

    # img = img.astype(np.float64)
    # img[...,0] = (img[...,0] - means[0]) * scales[0] * fix_scale
    # img[...,1] = (img[...,1] - means[1]) * scales[1] * fix_scale
    # img[...,2] = (img[...,2] - means[2]) * scales[2] * fix_scale

    img = img.astype(np.int8)

    return img


"""
## DPU Runner
"""
@vai_tracepoint
def runRetinaNet(runner: "Runner", img_paths, input_scale, cnt, decoder = None):
    # Get tensor
    input_tensors  = runner.get_input_tensors()
    output_tensors = runner.get_output_tensors()
    
    input_ndim = tuple(input_tensors[0].dims)
    # pre_output_size = int(output_tensors[0].get_data_size() / input_ndim[0])

    bboxes_output_ndim = tuple(output_tensors[0].dims)
    bboxes_output_fixpos = output_tensors[0].get_attr("fix_point")
    bboxes_output_scale = 1/(2**bboxes_output_fixpos)

    scores_output_ndim = tuple(output_tensors[1].dims)
    scores_output_fixpos = output_tensors[1].get_attr("fix_point")
    scores_output_scale = 1/(2**scores_output_fixpos)

    n_of_images = len(img_paths)

    count = 0
    times = []
    while count < cnt:
        run_size = input_ndim[0]

        """prepare batch input/output """
        input_data = [np.empty(input_ndim, dtype=np.int8, order="C")]
        bboxes_output_data = np.empty(bboxes_output_ndim, dtype=np.int8, order="C")
        scores_output_data = np.empty(scores_output_ndim, dtype=np.int8, order="C")
        
        """init input image to input buffer """
        for j in range(run_size):
            image_run = input_data[0]
            image_run[j, ...] = preprocessing(img_paths[(count + j) % n_of_images], input_scale).reshape(input_ndim[1:])
        """run with batch """
        ts = time.time()
        job_id = runner.execute_async(input_data, [bboxes_output_data, scores_output_data])
        runner.wait(job_id)
        tf = time.time()
        times.append(tf-ts) 

        if not decoder == None:
            decoder.call([bboxes_output_data, scores_output_data],
                         [bboxes_output_scale, scores_output_scale],
                         None)

        count = count + run_size
    
    print(1/(sum(times)/len(times)))



"""
## Obtain dpu subgrah
"""
def get_child_subgraph_dpu(graph: "Graph") -> List["Subgraph"]:
    """
    obtain the DPU subgraph
    """
    assert graph is not None, "'graph' should not be None."
    root_subgraph = graph.get_root_subgraph()
    assert (root_subgraph
            is not None), "Failed to get root subgraph of input Graph object."
    if root_subgraph.is_leaf:
        return []
    child_subgraphs = root_subgraph.toposort_child_subgraph()
    assert child_subgraphs is not None and len(child_subgraphs) > 0
    return [
        cs for cs in child_subgraphs
        if cs.has_attr("device") and cs.get_attr("device").upper() == "DPU"
    ]


def main(argv):
    test_images_path = "aveleda_test"
    thread_num = 3
    threadAll = []

    # reading the graph
    logging.info("Loading the model")
    graph = xir.Graph.deserialize("retinanet_quantized_qat_aveleda/retinanet_resnet50_aveleda_384_384_3.xmodel")
    subgraphs = get_child_subgraph_dpu(graph)
    assert len(subgraphs) == 1, "RetinaNet model can only have a single DPU graph. It has {}".format(len(subgraphs))
    all_dpu_runners = []
    for i in range(int(thread_num)):
        all_dpu_runners.append(vart.Runner.create_runner(subgraphs[0], "run"))

    input_fixpos = all_dpu_runners[0].get_input_tensors()[0].get_attr("fix_point")
    input_scale = 2**input_fixpos

    logging.info("Loading images")
    list_images = os.listdir(test_images_path)
    img = [os.path.join(test_images_path, img_path) for img_path in list_images] 
    # for img_path in list_images[:500]:
    #     img_full_path = os.path.join(test_images_path, img_path)
    #     img.append(preprocessing(img_full_path, input_scale))

    decoder = postprocessing.DecodePredictions()

    cnt = 360
    logging.info("Benchmarking the network")
    time_start = time.time()
    for i in range(int(thread_num)):
        thread = threading.Thread(target=runRetinaNet, args=(all_dpu_runners[i], img, input_scale, cnt, decoder))
        threadAll.append(thread)
    for x in threadAll:
        x.start()
    for x in threadAll:
        x.join()

    del all_dpu_runners

    time_end = time.time()
    time_total = time_end - time_start
    total_frames = cnt * int(thread_num)
    fps = float(total_frames / time_total)
    logging.info(
        "FPS=%.2f, T=%.2f ms, total frames = %.2f, time=%.6f seconds",
        fps, 1/fps*1000, total_frames, time_total
    )



if __name__ == '__main__':
    main(sys.argv)
