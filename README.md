# Xilinx Adaptive Computing Challenge 2021

## What problem are you going to solve?

The labour shortage and the world regulations move agriculture towards robotisation and precision agriculture. Martins, R. et al. (DOI:10.1007/978-3-030-30241-2_14) present an example of successful monitoring implementation in precision agriculture, where a mobile manipulator should move a spectrometer to a grape in a bunch of grapes for sensing. This procedure, like many others towards agricultural robotisation for tasks such as harvesting or spraying, demand precise and reliable detection of fruits in the crops. Besides, the detection of fruits should be fast enough to attend to the fruits' localisation on time and be competitive with human operators.

To solve the supra mentioned problem, we propose developing and applying a RetinaNet ResNet 50 object detector trained on a dataset of bunches of grapes in different growing stages and trunks. Using this strategy, we can approach problems like robot localisation in the vineyard based on landmarks (vines' trunks and grapes) and fruit localisation for different tasks like yield assessment, spraying, monitoring or harvesting.

This solution applies a custom acquired and manually labelled and preprocessed dataset in an object detector. We choose to use the RetinaNet ResNet 50 model over the TensorFlow 2 framework. The trained model is quantised and compiled using Vitis AI and executed on the Xilinx KV260 starter kit and ZCU104 Development Board.

## Solution description

### Dataset

For this work, we used a manually labelled dataset of vine trunks in different seasons of the year and green grapes in different growing stages. The dataset already contains augmentation in a total of 428.498 images. The dataset is publicly available at https://doi.org/10.5281/zenodo.5114141

The stated dataset has the following classes:

* trunk
* medium\_grape\_bunch
* tiny\_grape\_bunch

The images were acquired from multiple sources as stereo cameras (ZED camera, Intel RealSense), monocular cameras (Raspberry Pi Camera HQ), and thermal cameras (FLIR). In this way, we can get a robust dataset provided from different sources and with different featured images. 

For this dataset was applied different augmentation procedures as rotation, scaling, flipping, translation, and noise.
For training and evaluation purposes, the dataset was divided into three sets: training set, validation set and test set. The model was trained on the training set and was runtime evaluated on the validation set. The final assessment of the network was made on the test set. 

### Model Definition

To detect the fruits and the trunks in open-field scenes, we used the RetinaNet ResNet 50 object detector (DOI:10.1109/TPAMI.2018.2858826) inspired by the Keras implementation by Srihari Humbarwadi (https://keras.io/examples/vision/retinanet/). Because TensorFlow 2 for Vitis AI is only compatible with the functional API for deep learning models, we translated the sub-modelling model.
The full definition of the model can be found at retinanet.py.

### Training the model

After performing the due changes in the file, it is enough to execute the python script for training the model. The trainer will load the previously trained weights for the ImageNet dataset in the backbone. Still, the classifier and the regressor weights will be randomly initialised based on a normal distribution.

$ python3 train.py

We used the focal loss to train the model, as coded by Srihari Humbarwadi. The loss function is minimised by the stochastic gradient descendent (SGD). Because the loss function has two hyperparameters (alpha and gamma) and the SGD has two hyperparameters (learning rate and momentum), we used the Keras tuner, with the hyperband algorithm, to compute the best values that minimise the loss function.

## Diagrams
### Use case diagram

The KV260 starter kit receives sequential video images from an external sensor (straight from the camera or through a ROS topic). These images are sent to two detectors. The first detector, looks and segments the bunches of grapes. The second detector looks for the individual grapes. After, the system tries to cluster the grapes of each bunch and count the number of visible grapes per bunch. 

![Use case diagram](docs/diagrams/out/Use%20case%20diagram.png)

### Activity Diagram

![Activity diagram](docs/diagrams/out/Activity%20%20Overview%20Diagram.png)
