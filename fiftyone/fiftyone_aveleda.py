import sys
import fiftyone as fo
import fiftyone.zoo as foz
from collections import defaultdict
import json
from tqdm import tqdm
from pathlib import Path

import logging
logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

def evaluate_results(dataset, name, plot = False):
    logging.info("Evaluating %s", name)

    eval_key = "eval_{}".format(name)

    results = dataset.evaluate_detections(
        name,
        gt_field="ground_truth",
        eval_key=eval_key,
        compute_mAP=True
    )

    eval_patches = dataset.to_evaluation_patches(eval_key)

    print("[REPORT] Evaluating patches")
    print(eval_patches.count_values('type'))

    print("[REPORT] Aggregated metrics")
    results.print_report()
    print("mAP: {}".format(results.mAP()))

    if plot:
        plot_cm = results.plot_confusion_matrix()
        plot_cm.show()

        plot_pr = results.plot_pr_curves()
        plot_pr.show()

    bounds = dataset.bounds("{}.detections.confidence".format(name))
    print(bounds)

    return results

def load_predictions(dataset, name, filename, format = "corners"):
    logging.info("Loading predictions %s to %s", filename, name)
    with open(filename, 'r') as f:
        predictions_dict = json.load(f)

    ids = defaultdict(list)
    for sample in tqdm(dataset):
        ids[Path(sample.filepath).stem] = sample.id

    predictions = defaultdict(list)
    with fo.ProgressBar(len(predictions_dict)) as pb:
        for prediction_dict in pb(predictions_dict):
            bbox = prediction_dict["bbox"]
            if format == "corners":
                bbox [2] = bbox[2] - bbox[0]
                bbox [3] = bbox[3] - bbox[1]
            elif format == "cxcywh":
                bbox [0] = bbox[0] - bbox[2]/2
                bbox [1] = bbox[1] - bbox[3]/2
            predictions[ids[prediction_dict["image_id"]]].append(
                fo.Detection(
                    label = prediction_dict["category_name"],
                    bounding_box = bbox,
                    confidence = prediction_dict["score"]
                )
            )

    with fo.ProgressBar() as pb:
        for sample in pb(dataset):
            if len(predictions[sample["id"]]) > 0:
                sample[name] = fo.Detections(detections=predictions[sample["id"]])
                sample.save()
            
    return dataset


def load_dataset():
    name = "Aveleda"
    dataset_dir = "Aveleda/splits/test"

    logging.info("Loading Test Aveleda set")
    dataset = fo.Dataset.from_dir(
        dataset_dir = dataset_dir,
        dataset_type = fo.types.VOCDetectionDataset,
        name = name,
        data_path = "JPEGImages",
        labels_path = "Annotations",
    )

    print(dataset)
    # print(dataset.head())

    return dataset

def main(args):

    dataset = load_dataset()

    float_predictions = "float_predictions"
    dataset = load_predictions(dataset, float_predictions, "float_results.json")
    foat_results = evaluate_results(dataset, float_predictions, plot=False)

    ptq_predictions = "ptq_predictions"
    dataset = load_predictions(dataset, ptq_predictions, "ptq_results.json")
    ptq_results = evaluate_results(dataset, ptq_predictions, plot=False)

    dpu_ptq_predictions = "dpu_ptq_predictions"
    dataset = load_predictions(dataset, dpu_ptq_predictions, "dpu_ptq_results.json", format="xywh")
    dpu_ptq_results = evaluate_results(dataset, dpu_ptq_predictions, plot=True)

    # pqt_ft_predictions = "pqt_ft_predictions"
    # dataset = load_predictions(dataset, pqt_ft_predictions, "quantized_pqt_ft_detections.json")
    # pqt_results = evaluate_results(dataset, pqt_ft_predictions)

    # dpu_pqt_ft_predictions = "dpu_ptq_ft_predictions"
    # dataset = load_predictions(dataset, dpu_pqt_ft_predictions, "results.json", format="xywh")
    # dpu_pqt_results = evaluate_results(dataset, dpu_pqt_ft_predictions, plot=True)

    session = fo.launch_app(dataset, port=8002, address="0.0.0.0")
    session.wait()

if __name__ == "__main__":
    main(sys.argv)
